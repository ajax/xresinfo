#include <X11/Xlib.h>
#include <X11/extensions/XRes.h>

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>

static void
die(const char *msg, ...)
{
    va_list args;
    va_start(args, msg);
    vfprintf(stderr, msg, args);
    va_end(args);
    exit(1);
}

static void
print_size_spec(Display *dpy, XResResourceSizeSpec *size)
{
    printf("0x%08lx %-24s %10ld %10ld %10ld\n",
	   size->spec.resource,
	   XGetAtomName(dpy, size->spec.type),
	   size->bytes,
	   size->ref_count,
	   size->use_count);
}

int main(void)
{
    Display *dpy;
    if (!(dpy = XOpenDisplay(NULL)))
	die("Could not open display\n");

    int event, error;
    if (!XResQueryExtension(dpy, &event, &error))
	die("Resource extension not available\n");

    int major, minor;
    if (!XResQueryVersion(dpy, &major, &minor))
	die("Could not query version\n");

    if (major < 1 || (major == 1 && minor < 2))
	die("Need Resource extension >= 1.2 (got %d.%d)\n", major, minor);

    XResClientIdSpec spec = {
	.client = None,
	.mask = XRES_CLIENT_ID_XID_MASK,
    };
    long num;
    XResClientIdValue *ids;
    if (XResQueryClientIds(dpy, 1, &spec, &num, &ids))
	die("Failed to query clients\n");

    for (int i = 0; i < num; i++) {
	XResResourceIdSpec res = {
	    .resource = None,
	    .type = None,
	};
	long res_num;
	XResResourceSizeValue *size;
	if (XResQueryResourceBytes(dpy, ids[i].spec.client, 1, &res,
				   &res_num, &size))
	    continue; /* XXX */

	printf("Client %d (resource base 0x%08lx):     Bytes   Refcount  Use count\n", i, ids[i].spec.client);
	for (int j = 0; j < res_num; j++) {
	    XResResourceSizeValue *val = &size[j];

	    print_size_spec(dpy, &val->size);

	    for (int k = 0; k < val->num_cross_references; k++)
		print_size_spec(dpy, &val->cross_references[k]);
	}
    }

    return 0;
}
